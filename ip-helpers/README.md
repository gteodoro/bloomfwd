# Helpers

Some helper programs.

Note that `cpe.c` and `cpe_v6.c` must be linked to the appropriate version of
`bloomfwd.c` and `prettyprint.c`. Files ending in `*.hs` must be compiled using
the `ghc` Haskell compiler or executed directly using `runhaskell/ghci`.
